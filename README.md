# fundamentals

Even if you know these fundamentals, it can be helpful to see them used in practice.

Each module is made up of these submodules:
1. Refactor - Practice implementing the fundamental with existing code (suggestions provided)
2. New Feature - Implement a new feature using the fundamental to make the provided tests pass
3. Discussion - Questions to make you think

We will apply these fundamentals to an ongoing project.

## Martina's Meats
Martina's Meats opened in 2009 with just Martina and her two brothers. At the time, they handled procurement and invoicing for only a single client. Since then, the business has expanded and now employs over 200 people with over 30 clients! All is not rosy, though. Since Martina's opened, employees have been using an archaic, text only procurement/invoicing system to do their jobs. After coming into some money by selling an unseemly amount of chicken gizzards, Martina hires your consulting firm to replace their current system. 

## Get Started
When you're ready, run `git checkout module1-warmup` to get started.